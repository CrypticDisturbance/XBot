const Discord = require('discord.js');
const moment = require('moment');
const { token, prefix } = require('./settings.json');

const client = new Discord.Client({ disableEveryone: false });

var log = {
    info: `[INFO] [${moment().format('HH:mm:ss')}]`,
    warn: `[WARN] [${moment().format('HH:mm:ss')}]`,
    error: `[ERR] [${moment().format('HH:mm:ss')}]`
}

client
    .on ('ready',() => {
        console.log(`${log.info} ${client.user.tag} is ready! Prefix: ${prefix}`);
    })
    .on('disconnect', () =>{
        console.log(`${log.error} The bot got disconnected from Discord`)
    })
    .on('reconnecting', () => {
        console.log(`${log.warn} The bot lost connection to Discord. Reconnecting...`)
    })
    .on('guildDelete', guild => {
        console.log(`${log.info} The bot left ${guild.name}`)
    })
    .on('guildCreate', guild => {
        console.log(`${log.info} The bot joined ${guild.name}`)
    })
    .on ('message', message => {
        if (message.author.equals(client.user)) return;
        if (!message.content.startsWith(prefix)) return;
    
        var args = message.content.substring(prefix.length).split(" ");
    
        switch (args[0].toLowerCase()) {
            case "ping":
                message.channel.send(`:ping_pong: API response time: ${client.ping} ms`)
                break;
        }
    });

client.login(token);